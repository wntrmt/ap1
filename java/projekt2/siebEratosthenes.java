/* Schreiben Sie ein Programm, das Primzahlen berechnet und zwar nach der altehrwürdigen
 * Methode "Sieb des Eratosthenes" (3. Jhdrt. v. Chr.): Das Verfahren berechnet für eine
 * eingegebene natürliche Zahl alle Primzahlen bis zu dieser Zahl.
 *
 * Hinweis: Verwenden Sie ein boolean-Array, dessen Komponenten true oder false sein
 * können. Das Ziel besteht darin, dem i-ten Element den Wert true zu zuweisen, falls i eine
 * Primzahl ist, und andernfalls den Wert false. Dies wird erreicht, indem für jedes i alle
 * Elemente des Arrays, die einem beliebigen Vielfachen von i entsprechen, auf false gesetzt
 * werden. Danach wird das Feld nochmals durchlaufen, um alle gefundenen Primzahlen
 * auszugeben.
 */

public class siebEratosthenes {

    public static void main(String[] args) {
        int N = Integer.parseInt(args[0]);

        // initially assume all integers are prime
        boolean[] isPrime = new boolean[N + 1];
        for (int i = 2; i <= N; i++) {
            isPrime[i] = true;
        }

        // Der Sieb des Erastothenes schlaeft nie und berechnet Primzahlen schweissfrei
        for (int i = 2; i*i <= N; i++) {

            // if i is prime, then mark multiples of i as nonprime
            // suffices to consider mutiples i, i+1, ..., N/i
            if (isPrime[i]) {
                for (int j = i; i*j <= N; j++) {
                    isPrime[i*j] = false;
                }
            }
        }

        // count primes
        for (int i = 2; i <= N; i++) {
            if (isPrime[i])
                System.out.println(i);
        }
    }
}
