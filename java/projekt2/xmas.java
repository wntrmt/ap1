/* Es soll ein Java-Programm geschrieben werden, welches es ermöglicht, nach der Eingabe
 * einer Zahl, welche als Höhe in Zeilen dient, einen Tannenbaum zu zeichnen.
 */

import java.util.Scanner;

public class xmas {

    // Anstelle eines globalen Integerwerts muss in der main() eine Nutzereingabe implementiert werden.
    // static int height = 5;

    // Unsere input Klasse 'in'
    private static Scanner in;

    /* Es macht nur dann Sinn, einen Tannenbaum zu zeichnen, wenn seine Höhe
     * mindestens zwei Zeilen beträgt. Deshalb muss die Eingabe auf >=2 beschränkt
     * werden. Schreiben Sie eine Main-Methode, in der die Höhe des Baumes eingelesen
     * werden kann. Es dürfen nur Zahlen eingegeben werden, die größer als Eins sind.
     * Bei einer Fehleingabe, muss die Aufforderung eine Zahl einzugeben erneut
     * erscheinen. Wenn die Eingabe korrekt ist, muss die Methode „drawTree(h)“
     * aufgerufen werden, in der der Parameter „h“ für die zuvor eingegebene Zahl steht.
     * Die Implementierung der Methode erfolgt in der nächsten Teilaufgabe.
     */

    public static void main(String[] args) {

        // Wir initialisieren die in-Klasse
        in = new Scanner(System.in);

        // Wir holen uns einen Integer vom Nutzer fuer die Hoehe unseres Baums
        System.out.printf("Bitte geben Sie eine Ganzzahl fuer die Hoehe des Weihnachtsbaums an.");
        int height = in.nextInt();

        // Wir zeichnen unseren Weihnachtsbaum in der Hoehe 'height'...
        for (int h = 0; h < height; h++) {

            drawTree(h, height);
                // ...und brechen nach jeder vollendeten Reihe um
                System.out.println();

            }

        // Hier koennten wir ein optionales Parameter an die Methode drawTree uebergeben.
        // Existiert dieses Parameter, dann wird anstelle eines * ein # gezeichnet
        drawTree(0, height);
    }

    /* Implementieren Sie die Methode „drawTree(int height)“, die die eigentliche
     * Programmlogik beinhaltet. Die Methode hat keinen Rückgabewert. Diese Methode
     * soll einen Tannenbaum, mit der als Parameter übergebenen Höhe, zeichnen. Zeichnen
     * Sie zuerst die Baumkrone. Als letzter Schritt soll der Baumstamm gezeichnet werden.
     */

    private static void drawTree(int h, int height) {

        // startIndex und endIndex refaktorisieren
        int startIndex = height-h;
        int endIndex = height+h;

        for (int i = 1; i <= endIndex; i++) {

            if (i < startIndex)
                System.out.print(" ");
            else
                if (h == 0)
                    System.out.print("#");
                else
                    System.out.print("*");
        }
    }
}
