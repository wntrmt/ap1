/* Binaersuche
 *
 * Aktuelle Implementierung erfordert eine Nutzereingabe fuer den Array.
 * Die Eingabe muss sortiert erfolgen.
 *
 * @Todo: Entwickeln und testen Sie bitte eine Java-Methode binsearch, die eine binäre Suche auf
 * einem Integer-Array realisiert. Die Methode gibt die Position eines gesuchten Wertes im
 * int-Array zurück bzw. einen negativen Wert, wenn der Wert nicht vorkommt.
 *
 **/

import java.util.Scanner;
import java.util.Arrays;

class binarySearch {

    // Unsere rekursive binaere Suche
    private static int binSearch(int[] sortedArray, int searchElement) {

        // Wir initialisieren unsere variablen fuer die array-positionen
        int left = 0,
            right = sortedArray.length -1,
            middle = 0;

        // Wir bleiben in unserer schleife...
        do {
            middle = (left + right) / 2;
            if (sortedArray[middle] < searchElement) {
                left = middle + 1;
            } else {
                right = middle - 1;
            }

        // ...solange linker wert kleiner oder gleich rechts und array-mitte ungleich gesuchter wert
        } while ((left <= right) && (sortedArray[middle] != searchElement));

        // Wenn wir unser element finden dann...
        if (sortedArray[middle] == searchElement) {
            // ...geben wir dessen position zurueck
            return middle;
        } else {
            // ...ansonsten liefern wir einen negativen wert
            return -1;
        }
    }

   public static void main(String args[]) {

        // @Todo: Klassen fuer Nutzereingabe und Arraysortierung nachziehen
        int[] array = {1,2,3,4,5,6,7,8,9,10};
        int key = 7;
        int left = 1;
        int right = 10;
        int ergebnis = binSearch(array, key);
        System.out.println("Der Wert " + key + " befindet sich auf Position a[" + ergebnis + "]");
   }

}
