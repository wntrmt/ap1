/* Schreiben Sie ein Programm, das ein int-Array mit 10000 Speicherplätzen reserviert und
 * speichern Sie dort 10000 Zufallszahlen im Bereich von 0  9 ab. In einem weiteren Array
 * der Größe 10 zählen Sie nun wie oft jede Zahl gezogen wurde und benutzen Sie die Zahl
 * selbst als Index für das Zählerarray. Das Ergebnis geben Sie bitte als Tabelle so aus:
 *
 * Zahl | Häufigkeit
 * ------------------
 * 0    | 1000
 * 1    | 1005
 * 2    | 987
 *
 * Zur Lösung der Aufgabe benutzen Sie den Methodenaufruf Math.random() der Ihnen
 * Zufallszahlen im Bereich von 0.0 <= n < 1.0 zieht und erzeugen Sie sich damit eine
 * Zufallszahl im Bereich von 0 <= n < 10.
 */

class countArray {

    public static void main(String args[]) {

        // Unser Integer-Array-Set
        int[] sampleArray = new int[10000],
                     keys = {0,1,2,3,4,5,6,7,8,9},
              randomArray = inputArray(sampleArray); // Wir rufen unsere inputArray Methode auf und uebergeben unsere Parameter

        // Unser zweiter Methodenaufruf zaehlt das Vorkommen der 2. Methodenparameters im uebergebenen Array
        int[] elementOccurences = countArray(randomArray, keys);

        // Unsere Tabelle
        System.out.println("Zahl | Häufigkeit");
        System.out.println("-----------------");
        for(int i = 0; i < elementOccurences.length; i++) {
            System.out.println(keys[i] + "    | " + elementOccurences[i]);
        }

    }

    // Unsere Array-Anlegen-Methode
    public static int[] inputArray(int[] sampleArray) {
        for(int i = 1; i < sampleArray.length - 1; i++) {

            // Wir generieren unsere Zufallszahl zwischen 1 und 10 und runden diese via Integer Typecast
            int randomValue = (int)(Math.random() * 10);

            // Wir schreiben den Wert unserer Zufallszahl in unseren sampleArray[i]
            sampleArray[i] = randomValue;
        }

        return sampleArray;
    }

    // Unsere Zaehlmethode
    public static int[] countArray(int[] randomArray, int[] keys) {

        int count = 0;
        int arraySize = keys.length;
        int[] combinedCount = new int [arraySize];

        for (int i = 0; i < keys.length; i++) {
            for (int j = 0; j < randomArray.length; j++) {
                if (randomArray[j] == keys[i]) {
                    count++;
                }
            }

            // Unser Schluessel-Array wird je Element mit der korrespondierenden Anzahl der Elementvorkommnisse befuellt
            combinedCount[i] = count;

            // Wir setzen den Counter zurueck auf Null, sonst waechst dieser mit jeder Iteration
            count = 0;
        }

        return combinedCount;
    }
}
