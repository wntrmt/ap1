/* Schreiben Sie ein main -Programm, das 5 Array-Elemente ueber die Tastatur
 * einliest (Integer-Zahlen) und die kleinste Zahl ermittelt. Benutzen Sie noch keine Funktionen!
 */

import java.util.Scanner;

public class KleinsteZahl {

    private static Scanner in;

    public static void main (String[] args) {

        /* Wir initialisieren die in-Klasse */
        in = new Scanner(System.in);

        System.out.println("Wieviele Zahlen soll ihr Array enthalten?");
        int ArrayGroesse = in.nextInt();
        ArrayWerte = new double[ArrayGroesse];

        for (int i = 1; i < ArrayGroesse; i++) {
            System.out.printf("Bitte geben Sie die Zahl ein\n");
        }
    }
}
