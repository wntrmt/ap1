/* Beispiel fuer Eingabe mit util.Scanner */

import java.util.Scanner;

public class Adder {

    private static Scanner in;

    public static void main(String[] args) {

        in = new Scanner(System.in);

        System.out.print("1. Zahl: ");
        double a = in.nextDouble();
        System.out.print("2. Zahl: ");
        double b = in.nextDouble();
        System.out.printf("%.4f + %.4f = %6.4f%n", a, b, a + b);

    }

}
