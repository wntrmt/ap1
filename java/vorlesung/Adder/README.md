# Neue Objekte aus dieser Lerneinheit

java.util
Klasse Scanner
**java.util.Scanner**

Ein einfacher Text-Scanner der Primitives und Strings mit Hilfe regulaerer Ausdruecke parsen kann.

Der folgende Code erlaubt einem Nutzer beispielsweise eine Zahl von System.in auszulesen:

~~~
Scanner sc = new Scanner(System.in);
int i = sc.nextInt();
~~~

Methoden:

next()
nextLine()
nextBoolean()
nextInt()
nextLong()
nextFloat()
nextDouble()
