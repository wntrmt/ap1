/* Schreiben Sie ein Java-Programm, das ganzzahlige Werte in einem Array der Größe nach sortiert.
 * Die Größe des Arrays soll während der Laufzeit festgelegt werden. Verwenden Sie einen beliebigen
 * Sortieralgorithmus aus der Literatur. Den Algorithmus müssen Sie erläutern können. Sie müssen
 * auch den Aufwand des Algorithmus nennen können: Wieviele Schritte benötigt er für das Sortieren
 * von n Werten? Machen Sie sich mit dem Begriff O-Notation vertraut.
 *
 * Anmerkung zur Loesung: Wir verwenden die Klasse 'Arrays' und deren Methode 'sort(int[] a)' zur
 * Sortierung des nutzerdefinierten Arrays. Laut Java API-Dokumentation handelt es sich dabei um
 * einen modifizierten quickSort Algorithmus mit O(n*log(n))
 */

import java.util.Scanner;
import java.util.Arrays;

public class arraySort {

    // Unsere input Klasse 'in'
    private static Scanner in;

    public static void main(String[] args) {

        // Wir initialisieren die in-Klasse
        in = new Scanner(System.in);

        System.out.printf("\n##############################");
        System.out.printf("\n# Sortieralgorithmen in Java #");
        System.out.printf("\n##############################\n");
        System.out.printf("\nBitte treffen Sie eine Auswahl");
        System.out.printf("\n1. Quicksort via 'Arrays.sort()'");
        System.out.printf("\n2. Eigener Bubblesort-Algorithmus");
        System.out.printf("\nWelchen Sortieralgorithmus moechten Sie anwenden? ");
        int menueAuswahl = in.nextInt();

        int[] meinArray = arrayAnlegen();

        switch(menueAuswahl) {

            case 1:
                quickSort(meinArray);
                break;

            case 2:
                bubbleSort(meinArray);
                break;

            case 3:
                System.out.printf("Beende");
                break;

            default:
                System.out.printf("Ungueltige Eingabe. Beende");
                break;
        }
    }

    // Unsere Methode zum Anlegen eines Arrays
    private static int[] arrayAnlegen() {

        System.out.printf("Wieviele Elemente soll ihr Array enthalten? ");
        int arrayGroesse = in.nextInt();
        int[] arrayWerte = {};

        if( arrayGroesse <= 1 ) {

            System.out.printf("Fehler 1. Ihr Array muss fuer eine Sortierung mindestens 2 Elemente enthalten.");

        } else {

            arrayWerte = new int[arrayGroesse];
            int n = arrayWerte.length;

            // Wir holen uns die Array-Elementwerte vom Nutzer
            for (int i = 0; i < n; i++) {
                System.out.printf("Bitte geben Sie den Wert des %d-ten Elements ein: ", i+1);
                arrayWerte[i] = in.nextInt();
            }

            // Als Referenz geben wir den Array einmal unsortiert aus
            System.out.printf("Sie haben folgende Array-Elemente eingegeben\n");
            for (int i = 0; i < n; i++) {
                System.out.printf("%d ", arrayWerte[i]);
            }
        }
        return arrayWerte;
    }


    // Unser Arrays.sort() Methodenaufruf
    private static void quickSort(int[] arrayWerte) {

        // Wir sortieren unseren Array via 'Arrays.sort()' und geben diesen erneut aus
        Arrays.sort(arrayWerte);
        System.out.printf("\nIhre Array-Elemente haben sortiert die folgende Reihenfolge\n");
        for (int i = 0; i < arrayWerte.length; i++) {
            System.out.printf("%d ", arrayWerte[i]);
        }
        System.out.printf("\n");
    }

    // Unser Bubblesort Algo
    private static void bubbleSort(int[] arrayWerte) {

        int n = arrayWerte.length;
        int temp = 0;

        // Hier entsteht unsere Komplexitaet von O(n^2)
        for(int i = 0; i < n; i++) {
            for(int j = 1; j < (n-i); j++) {

                if(arrayWerte[j-1] > arrayWerte[j]) {
                    // Hier vertauschen wir die Elemente indem wir den linken Wert zunaechst in die Hilfsvariable temp schreiben
                    temp = arrayWerte[j-1];
                    // dann den (kleineren) rechten Wert nach links schreiben
                    arrayWerte[j-1] = arrayWerte[j];
                    // und schliesslich den kleineren Wert in die Hilfsvariable schreiben
                    arrayWerte[j] = temp;
                }
            }
        }

        System.out.printf("\nIhre Array-Elemente haben sortiert die folgende Reihenfolge\n");
        for (int i = 0; i < n; i++) {
            System.out.printf("%d ", arrayWerte[i]);
        }
    }
}
