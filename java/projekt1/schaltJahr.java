/* Schreiben Sie ein Programm, das berechnet ob es sich bei einem eingelesenen Wert
 * um ein Schaltjahr handelt oder nicht. Ein Schaltjahr liegt vor, wenn die
 * Jahreszahl durch 4, aber nicht durch 100, oder aber durch 400 teilbar ist.
 */

import java.util.Scanner;

public class schaltJahr {

    // Unsere input Klasse 'in'
    private static Scanner in;

    public static void main(String[] args) {

        // Wir initialisieren die in-Klasse
        in = new Scanner(System.in);

        System.out.printf("Bitte geben Sie eine Jahreszahl ein, um zu ueberpruefen ob es sich um ein Schaltjahr handelt: ");
        int schaltJahr = in.nextInt();
        String verb = new String("war");

        if (schaltJahr >= 2015) {
            verb = "ist";
        }

        if (schaltJahr % 4 == 0 && schaltJahr % 100 != 0 || schaltJahr % 400 ==0) {

            System.out.printf("Das Jahr %d " + verb + " ein Schaltjahr\n", schaltJahr);

        } else {

            System.out.printf("Das Jahr %d " + verb + " kein Schaltjahr\n", schaltJahr);

        }
    }
}
