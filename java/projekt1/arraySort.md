# Quicksort

# Bubblesort

Ein Bubblesort-Algorithmus wandert vom ersten bis zum letzten Element -1
eines Arrays. Dabei vergleicht der Algorithmus das aktuelle mit dem
naechsten Element und vertauscht diese miteinander sobald das naechste
Element groesser als das aktuelle ist.

Nach dem ersten kompletten Durchgang steht das groesste Element des
Arrays an letzter Stelle. Dieser Vorgang wird nun solange wiederholt bis
keines der Elemente groesser ist als sein Nachfolger.

Ein Bubblesort-Algorithmus hat eine Komplexitaet von O(n²)
