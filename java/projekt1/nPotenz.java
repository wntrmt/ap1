/* Schreiben Sie ein Java-Programm, das die n-te Potenz einer natürlichen Zahl x rekursiv berechnet.
 * In der main-Methode soll die Methode potenz aufgerufen werden, genau so, wie Sie es in C gelernt haben.
 */

import java.util.Scanner;

public class nPotenz{

    // Unsere input Klasse 'in'
    private static Scanner in;

    // Unsere Potenzierungsmethode
    private static int potenz(int x, int n) {

        // Falls n = 0, dann ist unser Ergebnis immer 1
        if(n == 0)
            return 1;
        // Ansonsten rufen wir unsere Methode 'potenz' rekursiv auf und geben das Ergebnis wieder
        else
            return x * potenz(x, n-1);

    }

    public static void main (String[] args) {

        // Wir initialisieren die in-Klasse
        in = new Scanner(System.in);

        System.out.printf("Geben Sie eine natuerlich Zahl als Basis ein: ");
        int x = in.nextInt();

        System.out.printf("Geben Sie eine natuerliche Zahl als Potenz ein: ");
        int n = in.nextInt();

        // Unser 'ergebnis' berechnet unsere Methode 'potenz'
        int ergebnis = potenz(x, n);
        System.out.printf("%d^%d = %d\n", x, n, ergebnis);

    }

}
