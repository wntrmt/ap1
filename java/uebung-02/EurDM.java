/* Schreiben Sie ein Java-Programm, das einen EUR-Betrag in DM umrechnet */

import java.util.Scanner;

public class EurDM {

    public static Scanner in;

    public static void main (String[] args) {

        /* Wir deklarieren unsere Variablen */
        double dm, eur, faktor = 1.95583;

        in = new Scanner(System.in);

        System.out.println("Das Jahr 2000. Wir rechnen Euro in DM um");

        System.out.println("Bitte geben Sie einen Euro-Betrag an");

        eur = in.nextDouble();
        dm = eur * faktor;

        System.out.printf("%.2f Euro betragen laut offiziellem Umrechnungsfaktor von %.2f %.2f DM", eur, faktor, dm);

    }

}
