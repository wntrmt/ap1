/* Schreiben Sie ein Java-Programm, das die Fläche und den Umfang eines Quadrats berechnet. */

import java.util.Scanner;

public class FlaecheQuadrat {

    public static Scanner in;

    public static void main (String[] args) {

        /* Wir deklarieren unsere Variablen */
        float Seitenlaenge, QuadratFlaeche, QuadratUmfang;

        /* Wir initialisieren die in-Klasse */
        in = new Scanner(System.in);

        System.out.println("Berechnung der Flaeche und des Umfangs eines Quadrats");

        System.out.printf("Bitte geben Sie die Seitenlaenge des Quadrats an: \n");
        Seitenlaenge = in.nextFloat();

        /* Unsere aufwendige Berechnung */
        QuadratFlaeche = Seitenlaenge * Seitenlaenge;
        QuadratUmfang = Seitenlaenge * 4;

        System.out.printf("Die Flaeche des Quadrats betraegt %.2f\n", QuadratFlaeche);
        System.out.printf("Der Umfang des Quadrats betraegt %.2f\n", QuadratUmfang);

    }

}
