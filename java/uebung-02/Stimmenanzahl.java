/* Schreiben Sie ein Java-Programm, das die Stimmenanzahl von 3 Parteien einliest und daraus
 * die Anteile der Stimmen in % berechnet und ausgibt.
 */

import java.util.Scanner;

public class Stimmenanzahl {

    public static Scanner in;

    public static void main (String[] args) {

        /* Wir deklarieren unsere Variablen */
        float StimmenA, StimmenB, StimmenC, GesamtStimmen;
        float AnteilA, AnteilB, AnteilC;

        /* Wir initialisieren die in-Klasse */
        in = new Scanner(System.in);

        System.out.println("Der Stimm-O-Mat. Berechnet Stimmen schweissfrei!\n");
        System.out.print("Bitte geben Sie die Zahl der erreichten Stimmen fuer Partei A an: ");
        StimmenA = in.nextInt();

        System.out.print("Bitte geben Sie die Zahl der erreichten Stimmen fuer Partei B an: ");
        StimmenB = in.nextInt();

        System.out.print("Bitte geben Sie die Zahl der erreichten Stimmen fuer Partei C an: ");
        StimmenC = in.nextInt();

        GesamtStimmen = StimmenA + StimmenB + StimmenC;
        AnteilA = 100 / GesamtStimmen * StimmenA;
        AnteilB = 100 / GesamtStimmen * StimmenB;
        AnteilC = 100 / GesamtStimmen * StimmenC;

        /* Wir geben unsere Berechnungen aus */
        System.out.printf("Partei A konnte %.2f Prozent der Stimmen erreichen%n", AnteilA);
        System.out.printf("Partei B konnte %.2f Prozent der Stimmen erreichen%n", AnteilB);
        System.out.printf("Partei C konnte %.2f Prozent der Stimmen erreichen%n", AnteilC);

    }

}
