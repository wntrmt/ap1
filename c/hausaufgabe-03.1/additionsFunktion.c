/* Schreiben Sie ein Programm, das zwei Zahlen einliest und anschließend diese addiert.
 * Legen Sie die Funktion für die Addition ausserhalb der main-Funktion an.
 */

#include <stdio.h>

// Wir deklarieren unsere Funktion
static float Addition(float a, float b);

int main() {

    // Wir deklarieren unsere Variablen
    float a, b;

    printf("Bitte geben Sie den Summanden a ein: ");
    scanf("%f", &a);
    printf("Bitte geben Sie den Summanden b ein: ");
    scanf("%f", &b);

    // Wir addieren die Summanden mit unserer Additionsfunktion
    float Ergebnis = Addition(a, b);

    // Wir geben unsere Summe aus
    printf("Das gerundete Ergebnis lautet %.2f\n", Ergebnis);

}

// Wir definieren unsere Funktion
static float Addition(float a, float b) {

    return a + b;

}
