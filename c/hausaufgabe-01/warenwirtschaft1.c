/*
 * Hausaufgabe 1 - Aufgabe 1
 * Ein Produkt in unserem System besteht aus einer Bezeichnung, einer Artikelnummer
 * und einem Einzelpreis. Es soll in dieser Übung ein Programm geschrieben werden,
 * das den Nutzer auffordert, die Informationen zu einem Produkt einzugeben und diese
 * danach auf der Textkonsole formatiert wieder auszugeben. Danach beendet sich das Programm.
 */

#include <stdio.h>

float calcGesamt(int menge, float preis) {
    return menge * preis;
}

int main() {

    /* Wir deklarieren unsere Variablen */

    int id, anzahl;
    char name[50];
    float preis, gesamt;
    
    /* Eingabeaufforderungen */

    printf("**********************\n"
            "* WWS Produkteingabe *\n"
            "**********************\n");

    puts("Bitte geben Sie eine Artikelnummer ein");
    scanf("%d", &id);
    puts("Bitte geben Sie einen Artikelnamen ein");
    scanf("%s", &name);
    puts("Bitte geben Sie die Artikelanzahl ein");
    scanf("%d", &anzahl);
    puts("Bitte geben Sie den Artikelpreis ein");
    scanf("%f", &preis);

    gesamt = calcGesamt(anzahl, preis);

    printf("\n========================");
    printf("\nArt-Nr: #%d", id);
    printf("\nName: %s", name);
    printf("\nPreis: %.2f", preis);
    printf("\nAnzahl: %d", anzahl);
    printf("\nGesamt: %.2f EURO", gesamt);
    printf("\n========================");

}
