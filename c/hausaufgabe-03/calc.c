#include <stdio.h>
#include "calc.h"

// Wir deklarieren unsere globalen structs
typedef struct EinArtikel {
    int ID, Menge;
    double Preis;
    char Name[128];
} ARTIKEL;

typedef struct EinKunde {
    int ID;
    char Vorname[128];
    char Nachname[128];
} KUNDE;

// Wir deklarieren unsere globalen Variablen
int AnzulegendeKunden,
    AnzulegendeArtikel,
    AbzufragendeKundenID,
    AbzufragendeArtikelID,
    i;

ARTIKEL meineArtikel[20];
KUNDE meineKunden[20];

// Unsere Menue-Funktion
static void Menue() {

    int choice = 0;

    do {

        puts("Bitte waehlen Sie eine Options aus");
        puts("[1] Neuen Artikel eingeben");
        puts("[2] Neuen Kunden eingeben");
        puts("[3] Einzelnen Artikel ausgeben");
        puts("[4] Ges. Produktliste ausgeben");
        puts("[5] Einzelnen Kunden ausgeben");
        puts("[6] Ges. Kundenliste ausgeben");
        puts("[7] Beenden");
        scanf("%i", &choice);

        switch(choice) {

            case 1:
                ArtikelAnlegen();
                break;
            case 2:
                KundenAnlegen();
                break;
            case 3:
                ArtikelAusgeben("einzeln");
                break;
            case 4:
                ArtikelAusgeben("alle");
                break;
            case 5:
                KundenAusgeben("einzeln");
                break;
            case 6:
                KundenAusgeben("alle");
                break;
            case 7:
                puts("Beende");
                break;
            default:
                puts("Ungueltige Eingabe");
        }

    } while(choice != 7);

}

static void ArtikelAnlegen() {

    int flag = 1;
    // Wir starten den ersten Durchlauf fuer die Artikeleingabe und holen Nutzer-Input am Ende
    do {

        meineArtikel[AnzulegendeArtikel].ID = AnzulegendeArtikel; // Auto-Increment Kundennummer
        printf("Bitte geben Sie einen Artikelnamen ein: ");
        scanf("%127s", meineArtikel[AnzulegendeArtikel].Name);
        printf("Bitte geben Sie die Artikelmenge ein: ");
        scanf("%i", &meineArtikel[AnzulegendeArtikel].Menge);
        printf("Bitte geben Sie den Artikeleinzelpreis ein: ");
        scanf("%lf", &meineArtikel[AnzulegendeArtikel].Preis);

        puts("Moechten Sie einen weiteren Artikel anlegen? (ja=1, nein=0)");
        scanf("%i", &flag);
        AnzulegendeArtikel++;

    } while(flag == 1);

}

static void ArtikelAusgeben(char range[6]) {

    if(range == "einzeln") {

        // Einzelnen Artikel via Artikelnummer ausgeben
        printf("Welchen Artikel moechten Sie ansehen? Artikelnummer >> ");
        scanf("%i", &AbzufragendeArtikelID);

        if(meineArtikel[AbzufragendeArtikelID].ID) {

            printf(" Art.-Nr: %i\n", meineArtikel[AbzufragendeArtikelID].ID);
            printf("Artikeln: %s\n", meineArtikel[AbzufragendeArtikelID].Name);
            printf("   Lager: %i\n", meineArtikel[AbzufragendeArtikelID].Menge);
            printf("   Preis: %.2f\n", meineArtikel[AbzufragendeArtikelID].Preis);

        } else {

            puts("Artikelnummer nicht gefunden");

        }

    } else {

        // Artikelliste ausgeben
        // Wir ueberpruefen ob mindestens ein Artikel existiert
        if(meineArtikel[1].ID) {

            puts("Artikelliste");
            puts("-------------------------------------------------------------------");
            puts("| Art.-Nr. |                        Titel |     Menge |     Preis |");
            puts("-------------------------------------------------------------------");
            for(i=0; i < AnzulegendeArtikel; i++) {
                printf("| %6i |", meineArtikel[i].ID);
                printf(" %30s |", meineArtikel[i].Name);
                printf(" %9i|", meineArtikel[i].Menge);
                printf(" %.2f|\n", meineArtikel[i].Preis);
                puts("-------------------------------------------------------------------");
            }

        } else {

            puts("Es existieren noch keine Artikeldaten");

        }

    }

}

static void KundenAnlegen() {

    int flag = 1;
    // Wir starten den ersten Durchlauf fuer die Kundeneingabe und holen Nutzer-Input am Ende
    do {

        meineKunden[AnzulegendeKunden].ID = AnzulegendeKunden; // Auto-Increment Kundennummer
        printf("Bitte geben Sie einen Vornamen ein: ");
        scanf("%127s", meineKunden[AnzulegendeKunden].Vorname);
        printf("Bitte geben Sie einen Nachnamen ein: ");
        scanf("%127s", meineKunden[AnzulegendeKunden].Nachname);

        puts("Moechten Sie einen weiteren Kunden anlegen? (ja=1, nein=0)");
        scanf("%i", &flag);
        AnzulegendeKunden++;

    } while(flag == 1);

}

static void KundenAusgeben(char range[6]) {

    if(range == "einzeln") {

        // Einzelnen Kunden via Kundennummer ausgeben
        printf("Welchen Kunden moechten Sie ansehen? Kundennummer >> ");
        scanf("%i", &AbzufragendeKundenID);

        if(meineKunden[AbzufragendeKundenID].ID) {

            printf("   Kd-Nr: %i\n", meineKunden[AbzufragendeKundenID].ID);
            printf(" Vorname: %s\n", meineKunden[AbzufragendeKundenID].Vorname);
            printf("Nachname: %s\n", meineKunden[AbzufragendeKundenID].Nachname);

        } else {

            puts("Kundennummer nicht gefunden");

        }

    } else {

        // Wir ueberpruefen ob mindestens ein Kunde existiert
        if(meineKunden[1].ID) {

            puts("Kundenliste");
            puts("----------------------------------------------------------------------------");
            puts("| Kd-Nr. |                        Vorname |                      Nachnname |");
            puts("----------------------------------------------------------------------------");
            for(i=0; i < AnzulegendeKunden; i++) {
                printf("| %6i |", meineKunden[i].ID);
                printf(" %30s |", meineKunden[i].Vorname);
                printf(" %30s |\n", meineKunden[i].Nachname);
                puts("----------------------------------------------------------------------------");
            }

        } else {

            puts("Es existieren noch keine Kundendaten");

        }

    }
}
