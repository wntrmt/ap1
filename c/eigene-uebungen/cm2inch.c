/* Write a program in C that receives as input a height in centimeters.
 * The problem will then calculate and display the following:
 * The height in feet and inches
 */

#include <stdio.h>

int main () {

    // wir definieren unsere variablen
    float cmHeight;

    printf("Please enter the height in centimeters: ");
    scanf("%f", &cmHeight);

    float inHeight = 2.54 * cmHeight;
    float ftHeight = inHeight / 12;

    printf("\nYou entered %.2fcm", cmHeight);
    printf("\nor %.2fin", inHeight);
    printf("\nor %.2fft", ftHeight);
    printf("\n");

    return 0;

}
