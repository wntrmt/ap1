/***********************************************
* Integer-Arrays als Funktionsparameter
***********************************************/

#include <stdio.h>

void einlesen(int a[], int n) {

    int i;

    for (i = 0; i < n; i++) {
        printf("\nBitte %i-tes Element eingeben: ", i);
        scanf("%i", &a[i]);
    }

}

void ausgeben(const int a[], int n) {

    int i;

    printf("\nDie %i Array-Elemente heißen: ", n);

    for (i = 0; i < n; i++) {
        printf("%i", a[i]);
    }

}
void addZahl(int *a, int n, int zahl) {

    int i;

    for (i = 0; i < n; i++) {
        a[i] = a[i] + zahl;
    }

}

int main() {

    const int groesse = 10;
    int meinArray[groesse];

    einlesen(meinArray, groesse);
    ausgeben(meinArray, groesse);
    addZahl(meinArray, groesse, 4);
    ausgeben(meinArray, groesse);

    return 0;
}
