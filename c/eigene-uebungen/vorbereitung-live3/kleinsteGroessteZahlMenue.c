/* Setzen Sie folgendes um:
 * Ein Auswahlmenue mit den Punkten 'Eingabe' und 'Kleinste Zahl ausgeben' sowie 'Groesste Zahl ausgeben'
 * Ein Nutzer soll 5 Zahlen eingeben und innerhalb dieser die kleinste bzw. groesste anzeigen lassen koennen.
 */

#include <stdio.h>

void Menue();
void ZahlenEingeben(int a[], int n);
void KleinsteZahl(int a[], int n);
void GroessteZahl(int a[], int n);
void ZahlSubtrahieren(int a[], int n);

int main() {
    Menue();
    return 0;
}

void Menue() {

    // Wir deklarieren unsere Variablen
    int auswahl,
        arrayGroesse = 5,
        zahlen[arrayGroesse];

    do {

        puts("Bitte treffen Sie eine Auswahl");
        puts("1. Zahlen eingeben");
        puts("2. Kleinste Zahl ausgeben");
        puts("3. Groesste Zahl ausgeben");
        puts("4. Beenden");

        scanf("%i", &auswahl);

        switch(auswahl) {

            case 1:

                puts("Sie haben 1 gewaehlt!");
                ZahlenEingeben(zahlen, arrayGroesse);
                break;

            case 2:

                puts("Sie haben 2 gewaehlt!");
                KleinsteZahl(zahlen, arrayGroesse);
                break;

            case 3:

                puts("Sie haben 3 gewaehlt!");
                break;

            case 4:

                puts("Beende!");
                break;

            default:

                puts("Ungueltige Eingabe!");
                break;

        }

    } while(auswahl != 4);
}

void ZahlenEingeben(int a[], int n) {

    int i;

    for (i = 0; i < n; i++) {

        printf("Bitte geben Sie die %i. Zahl ein: ", i+1);
        scanf("%i", &a[i]);

    }
}

void KleinsteZahl(int a[], int n) {

    int i,
        array_min = a[0];

    for (i = 0; i < n; i++) {

       if(a[i] < array_min) {

          array_min = a[i];

       }

    }

    printf("\nDie kleinste Zahl im Array ist %i ", array_min);

}

void GroessteZahl();
