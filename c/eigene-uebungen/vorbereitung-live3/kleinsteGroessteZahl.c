/* Einlesen und kleinste sowie groesste Zahl ausgeben */

#include <stdio.h>

// Wir deklarieren unsere Fuktionen
void ZahlEingeben();
void ZahlAusgeben();
void ZahlAddieren();
void KleinsteZahl();
void GroessteZahl();

int main() {

	int const GROESSE = 3,
			  SUMMAND = 1;

	int Zahlen[GROESSE],
		auswahl;

	do {

		puts("\nBitte treffen Sie eine Auswahl");
		puts("1. Zahlen eingeben");
		puts("2. Zahlen ausgeben");
		puts("3. Kleinste Zahl");
		puts("4. Groesste Zahl");
		puts("5. Zahl addieren");
		puts("6. Beenden");
		scanf("%i", &auswahl);

		switch(auswahl) {

			case 1:
				ZahlEingeben(Zahlen, GROESSE);
				break;
			case 2:
				ZahlAusgeben(Zahlen, GROESSE);
				break;				
			case 3:
				KleinsteZahl(Zahlen, GROESSE);
				break;				
			case 4:
				GroessteZahl(Zahlen, GROESSE);
				break;
			case 5:
				ZahlAddieren(Zahlen, GROESSE, SUMMAND);
				break;
			case 6:
				puts("Beende.");
				break;
			default:
				puts("Ungueltige Eingabe. Beende.")	;
				break;
		}

	} while( auswahl != 6 );

	printf("\n");

	return 0;
}

// Entsprechend C Best-Practices definieren wir unsere Variablen am Ende der Datei nach der eingaenglichen Deklaration

void ZahlEingeben(int a[], int n) {

	int i;

	for ( i = 0; i < n; i++ ) {
		printf("Bitte geben Sie die %i-te Zahl ein: ", i);
		scanf("%i", &a[i]);
	}
}

void ZahlAusgeben(int a[], int n) {

	int i;

	for ( i = 0; i < n; i++ ) {
		printf("\nDie %i-te Zahl lautet: %i", i, a[i]);
	}
}

void KleinsteZahl(int a[], int n) {

	// Wir nehmen an das a[0] das kleinste member beinhaltet
	int smallestMember = a[0],
		i;

	for ( i = 0; i < n; i++ ) {
		if ( a[i] < smallestMember ) {
			smallestMember = a[i];
		}
	}

	printf("\nDie kleinste Zahl in Ihrem Array hat den Wert %i", smallestMember);

}

void GroessteZahl(int a[], int n) {

	// Wir nehmen an das a[0] das groesste member beinhaltet
	int largestMember = a[0],
		i;

	for ( i = 0; i < n; i++ ) {
		if ( a[i] > largestMember ) {
			largestMember = a[i];
		}
	}

	printf("\nDie groesste Zahl in Ihrem Array hat den Wert %i", largestMember);

}

void ZahlAddieren(int *a, int n, int SUMMAND) {

	int i;

	for ( i = 0; i < n; i++ ) {
		a[i] += SUMMAND;
	}

}