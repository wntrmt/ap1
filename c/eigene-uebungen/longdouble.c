// Welchen Wertebereich hat ein long double?

#include <stdio.h>
#include <float.h>
#include <limits.h>

int main () {

    // Long
    printf("Minimaler Wert fuer ein long ist: %li\n", LONG_MIN);
    printf("Maximaler Wert fuer ein long ist: %li\n", LONG_MAX);

    // Float
    printf("Minimaler Wert fuer ein float ist: %f\n", FLT_MIN);
    printf("Maximaler Wert fuer ein float ist: %f\n", FLT_MAX);

    // Double
    printf("Minimaler Wert fuer ein double ist: %lf\n", DBL_MIN);
    printf("Maximaler Wert fuer ein double ist: %lf\n", DBL_MAX);

    // Long Double
    printf("Minimaler Wert fuer ein long double ist: %Le\n", LDBL_MIN);
    printf("Maximaler Wert fuer ein long double ist: %Le\n", LDBL_MAX);

    return 0;

}
