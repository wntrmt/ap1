/* Uebung zur Rekursion in C */

#include <stdio.h>

static long rekursivePotenz(int x, int n) {
    int y;
    if(n == 0)
        return 1;
    else
        y = x * rekursivePotenz(x, n - 1);
        printf("%i\n", y);
}

int main() {
    int zahl,
        potenz;

    puts("Bitte geben Sie die zu potenzierende Zahl ein");
    scanf("%i", &zahl);

    puts("Bitte geben Sie eine Potenz ein");
    scanf("%i", &potenz);

    rekursivePotenz(zahl, potenz);
    return 0;
}
