/*
 * Uebung 1 - Aufgabe 1
 * Programmieren Sie einen Einfachen Taschenrechner
 * Author: Marco Damm
 */

#include <stdio.h>
#include <stdlib.h>

/* Unsere Berechnungsfunktionen fuer alle Grundrechenarten damit die main() schoen sauber bleibt */

float addition(float in1, float in2) {
    return in1 + in2;
}

float subtraktion(float in1, float in2) {
    return in1 - in2;
}

float multiplikation(float in1, float in2) {
    return in1 * in2;
}

float division(float in1, float in2) {
    return in1 / in2;
}

int main() {

    int auswahl;
    float in1, in2, ergebnis;

    /* Um nur eine Text + Zeilenumbruch auszugeben ist puts besser geeignet als printf.
     * Der Compiler wandelt wo moeglich printf waehrend des Kompilierens ohnehin in puts um
     */
    puts("Dieser 'einfache Taschenrechner' erlaubt das Rechnen mit max. 2 Operanden und den Grundrechenarten. Geben Sie diese bitte im folgenden an.");

    printf("Waehlen Sie zunaechst eine Grundrechenart indem Sie die passende Ziffer eingeben:\n\n"
            "1. Addition\n"
            "2. Subtraktion\n"
            "3. Multiplikation\n"
            "4. Division\n"
            " >> ");

    scanf("%d", &auswahl);

    puts;
    puts("Geben Sie nun die erste Ziffer ein          >> ");
    scanf("%f", &in1);
    puts("Geben Sie als naechstes zweite Ziffer ein   >> ");
    scanf("%f", &in2);

    if(auswahl > 4 || auswahl < 1) {

        puts("Fehler 1: Bitte geben Sie eine Zahl zwischen 1 und 4 fuer Ihre Auswahl an.");
        exit(1);

    } else {
            
        switch(auswahl) {

            case 1:

                puts;
                ergebnis = addition(in1, in2);
                
                /* Wir formartieren unsere Ausgabe mit printf um sie besser lesbar zu machen */
                printf("Ihre Addition ergibt\n"
                        "  %.2f\n"
                        "+ %.2f\n"
                        "-------\n"
                        "  %.2f\n",
                        in1,
                        in2,
                        ergebnis);

                break;

            case 2:

                puts;
                ergebnis = subtraktion(in1, in2);
                
                printf("Ihre Subtraktion ergibt\n"
                        "  %.2f\n"
                        "- %.2f\n"
                        "-------\n"
                        "  %.2f\n",
                        in1,
                        in2,
                        ergebnis);

                break;

            case 3:

                puts;
                ergebnis = multiplikation(in1, in2);
                
                printf("Ihre Multiplikation ergibt\n"
                        "  %.2f x %.2f\n"
                        "--------------\n"
                        "         %.2f\n",
                        in1,
                        in2,
                        ergebnis);

                break;

            case 4:

                puts;
                ergebnis = division(in1, in2);
                
                printf("Ihre Division ergibt\n"
                        "  %.2f : %.2f\n = %.2f\n",
                        in1,
                        in2,
                        ergebnis);

                break;
        
        }

    }

    return 0;

}
