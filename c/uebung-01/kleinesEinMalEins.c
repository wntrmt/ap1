/*
 * Uebung 1 - Aufgabe 3
 * Geben Sie alle Reihen des kleinen Einmaleins aus
 * Author: Marco Damm
 */

#include <stdio.h>

int main() {

    int n, i;

    printf ( "Das kleine Einmaleins\n" );
    for ( i=1; i<=10;++i ) {
        for ( n=1; n<=9;++n ) {
            printf ( "%d * %d = %d\n", n, i, n*i );
        }
    }
    return 0;

}
