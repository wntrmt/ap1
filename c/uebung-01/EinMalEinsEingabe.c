/*
 * Uebung 1 - Aufgabe 2
 * Geben Sie fuer eine ueber die Tastatur abgefragte Ziffer das passende kleine Einmaleins aus
 * Author: Marco Damm
 */

#include <stdio.h>

int main() {
    int n, i;
    printf("Fuer welche Ziffer zwischen 1-9 soll das 'kleine Einmaleins' ausgegeben werden? ");
    scanf("%d",&n);
    for(i=1;i<=10;++i)
    {
        printf("%d * %d = %d\n", n, i, n*i);
    }
    return 0;
}
