/* Aufgabe 1: C-Programmierung
 * Schreiben Sie ein main -Programm, das 5 Array-Elemente einliest (Integer-Zahlen) und die
 * kleinste Zahl ermittelt. Benutzen Sie noch keine Funktionen!
 */

#include <stdio.h>

int main() {

    // Wir deklarieren unsere Variablen
    int i, a[5], array_min;

    for(i=0; i < 5; i++) {

        printf("Geben Sie die %i. Zahl ein: ", i+1);
        scanf("%i", &a[i]);

    }

    // Wir nehmen an dass das erste Array Element das kleinste ist
    array_min = a[0];

    for(i=0; i < 5; i++) {

        if(a[i] < array_min) {

            array_min = a[i];

        }

    }

    printf("\nDer kleinste Wert in Ihrem Array ist %i\n", array_min);

    return 0;

}
